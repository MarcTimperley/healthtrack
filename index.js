const http = require('http')
const path = require('path')
const fs = require('fs')
const config = require('./config.js')()
const port = process.env.PORT || config.port || 3000
const server = http.createServer()
server.on('request', (req, res) => {
  let url = req.url
  if (url === '/') url = 'index'
  if (url.indexOf('.') < 0) url += '.html'
  let contentType
  if (url.indexOf('.html')>1) contentType= 'text/html'
  if (url.indexOf('.css')>1) contentType= 'text/css' 
  if (url.match(/\.json$/)) contentType= 'text/json' 
  if (url.match(/\.js$/)) contentType= 'text/javascript'
  try { 
    let html = fs.readFileSync(path.join(__dirname, 'public', url))
    console.log(url, contentType)
    res.writeHead(200, { 'Content-Type': {contentType}})
    res.write(html)
  } catch (e) {
    console.log(e)
  }
  
  res.end()
})
server.listen(port)
