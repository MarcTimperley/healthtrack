const os = require('os')
module.exports = () => {
  return {
    appName: 'HealthTracker', // Name of the application
    port: 3000, // Express server port
    logPath: os.tmpdir() // file path for logging
  }
}
