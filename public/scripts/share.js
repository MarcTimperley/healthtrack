const ls = window.localStorage.getItem('healthTracker')
const lsO = JSON.parse(ls)
let html = ''
let totalRecords = 0
let totalSize=0
if (lsO) {
    for (let element of Object.entries(lsO)) {
        console.log(element)
        const records = element[1].time ?element[1].time.length :Array.isArray(element[1]) ?element[1].length :0
        totalRecords += records
        const size=JSON.stringify(element).length
        totalSize += size
        html += `<tr>
        <td>${element[0]}</td>
        <td class="text-right">${records.toLocaleString()}</td>
        <td class="text-right">${size.toLocaleString()}</td>
        </tr>`
    }
    html+=`<tr><td class="font-weight-bold">Total</td>
    <td class="text-right font-weight-bold">${totalRecords.toLocaleString()}</td>
    <td class="text-right font-weight-bold">${totalSize.toLocaleString()}</td>
    </tr>`
    $('#dataSummaryDetails').html(html)
} else {
    $('#dataSummary').addClass('invisible')
    $('#dataImport').addClass('invisible')
}

setupClickHandlers()

const setupClickHandlers = () => {
    $('#dataExport').on('click', (e) => {
        e.preventDefault()
    })
    $('#dataImport').on('click', (e) => {
        e.preventDefault()
    })
}