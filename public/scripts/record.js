let html = ''

const buildInput = (recordTitle) => {
  html += `<div class="form-group row">`
  html += `<label for="record_value_${recordTitle.name}" class="col-5 col-form-label">${recordTitle.name}</label>`
  html += `<div class="col-4">`
  html += `<input type="number" id="record_value_${recordTitle.name}" name="record_value_${recordTitle.name}" pattern="[0-9]*" inputmode="numeric" class="form-control value col-sm-10" value="${recordTitle.default}" min="${recordTitle.min}" max="${recordTitle.max}" step="${recordTitle.step}">`
  html += `</div>`
  html += `<span class="display-6 col-3">${recordTitle.units}</span>`
  html += `</div>`
}

const setupClickHandlers = () => {
  $('#record_submit').on('click', () => {
    $('.value').each((index, obj) => {
      saveRecords(obj.id.substring(13), obj.value)
    })
    $('#saveConfirmation').removeClass('invisible')
  })
  $('#saveConfirmation').on('click', () => {
    if (window.location.pathname.match(/\/healthtrack/)) {
      window.location.replace('/healthtrack/')
  } else { window.location.replace('/') }
    $('#saveConfirmation').addClass('invisible')
  })

}

const saveRecords = (category, value) => {
  let lsO = []
  if (ls) {
    lsO = JSON.parse(ls)
    if (!lsO[category]) {
      lsO[category] = {
        time: [],
        value: []
      }
    }
  } else {
    lsO = {
      [category]: {
        time: [],
        value: []
      }
    }
  }
  lsO[category].time.push(new Date())
  lsO[category].value.push(value)
  window.localStorage.setItem('healthTracker', JSON.stringify(lsO))
  ls = window.localStorage.getItem('healthTracker')
}

let ls = window.localStorage.getItem('healthTracker')
let lsO = {}
if (ls) {
  lsO = JSON.parse(ls)
  if (lsO.setup && lsO.setup.tracking) {
    for (let tracking of lsO.setup.tracking) {
      const recordTitle = recordTypes.find((obj) => { return obj.name === tracking.replace('-', ' ') })
      buildInput(recordTitle)
    }
    html += `<button class="btn btn-primary ml-2" id="record_submit">Submit</button><br/>
    <br />`
  }
}
$('#record_details').html(html)
setupClickHandlers()
