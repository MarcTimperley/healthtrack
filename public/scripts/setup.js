const initPage = () => {
    ls = window.localStorage.getItem('healthTracker')
    if (ls) {
        lsO = JSON.parse(ls)
    } else {
        lsO = { setup: { medicines: [] } }
    }
    if (!lsO.setup || !lsO.setup.medicines) {
        console.log('notsetup')
        lsO.setup = { medicines: [] }
        window.localStorage.setItem('healthTracker', JSON.stringify(lsO))
    }
    if (lsO.setup.name) $('#name').val(lsO.setup.name)
    let html = ``
    for (let recordType of recordTypes) {
        const trackName = recordType.name.replace(' ', '-')
        if (lsO && lsO.setup && lsO.setup.tracking && lsO.setup.tracking.indexOf(recordType.name) > -1) {
            html += `<input class="form-check-input" type="checkbox" value="${recordType.name}" id="${trackName}" checked>`
        } else {
            html += `<input class="form-check-input" type="checkbox" value="${recordType.name}" id="${trackName}">`
        }
        html += `<label class="form-check-label" for="${trackName}">${recordType.name}</label><br />`
    }
    $('#trackingEnabled').html(html)
    displayMedicines()
}

const displayMedicines = () => {
    html = ''
    for (let medicineName of lsO.setup.medicines) {
        if (medicineName) html += `<tr><td>${medicineName}</td></tr>`
    }
    $('#medicineList').html(html)
    lsO.setup.medicines.length === 0 ? $('#removeMed').addClass('invisible') : $('#removeMed').removeClass('invisible')
    setupClickHandlers()
}
const saveData = () => {
    const result = { name: $('#name').val(), dob: $('dob').val(), medicines: [], tracking: [] }
    $('#medicineList tr td').each((i, v) => {
        result.medicines.push(v.innerText)
    })
    $('#trackingEnabled :checked').each((i, v) => {
        result.tracking.push(v.value)
    })
    ls = window.localStorage.getItem('healthTracker')
    if (ls) {
        lsO = JSON.parse(ls)
    } else {
        lsO = {}
    }
    lsO.setup = result
    window.localStorage.setItem('healthTracker', JSON.stringify(lsO))
}

const saveMedicines = () => {
    window.localStorage.setItem('healthTracker', JSON.stringify(lsO))
    displayMedicines()
}

const setDummyData = () => {
    const time = Array.from({ length: 2500 }, () => new Date() - Math.floor(Math.random() * 1e10))
    const data = {
        'Temperature': {
            'time': time,
            'value': Array.from({ length: 2500 }, () => 35 + Math.floor(Math.random() * 4))
        },
        'Pain': {
            'time': time,
            'value': Array.from({ length: 2500 }, () => Math.floor(Math.random() * 10))
        },
        'Heart Rate': {
            'time': time,
            'value': Array.from({ length: 2500 }, () => 50 + Math.floor(Math.random() * 90))
        },
        'Glucose': {
            'time': time,
            'value': Array.from({ length: 2500 }, () => (39 + Math.floor(Math.random() * 35)) / 10)
        },
        'setup': {
            tracking: ['Temperature', 'Pain', 'Heart Rate', 'Glucose'], medicines: []
        }
    }

    window.localStorage.setItem('healthTracker', JSON.stringify(data))
    saveData()
}


const setupClickHandlers = () => {
    $('#medicineAdd').on('click', (e) => {
        e.preventDefault()
        if (!$('#medicineNew').val()) return null
        lsO.setup.medicines.push($('#medicineNew').val())
        $('#medicineNew').val('')
        saveMedicines()

    })
    $('#medicineList tr td').on('click', (e) => {
        e.preventDefault()
        lsO.setup.medicines = lsO.setup.medicines.filter(element => element !== e.target.innerText)
        saveMedicines()
    })
    $('#setupSubmit').on('click', (e) => {
        e.preventDefault()
        saveData()
        $('#saveConfirmation').removeClass('invisible')
        $('#saveConfirmation').on('click', () => {
            $('#saveConfirmation').addClass('invisible')
            if (window.location.pathname.match(/\/healthtrack/)) {
                window.location.replace('/healthtrack/')
            } else { window.location.replace('/') }
        })
    })
    $('#dummyData').on('click', () => {
        setDummyData()
        $('#saveConfirmation').removeClass('invisible')
    })
}

initPage()
setupClickHandlers()
