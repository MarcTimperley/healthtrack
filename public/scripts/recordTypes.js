const recordTypes = [{
    'name': 'Temperature',
    'min': 35,
    'default': 37.0,
    'max': 40,
    'step': 0.1,
    'units': '&deg;C',
    'enabled': true
},
{
    'name': 'Pain',
    'min': 0,
    'default': 0,
    'max': 10,
    'step': 1,
    'units': '',
    'enabled': true
},
{
    'name': 'Glucose',
    'min': 3,
    'default': 5.5,
    'max': 9,
    'step': 0.1,
    'units': 'mg/dL',
    'enabled': false
},
{
    'name': 'Heart Rate',
    'min': 50,
    'default': 80,
    'max': 160,
    'step': 5,
    'units': 'beats/min',
    'enabled': true
}
]