const ls = window.localStorage.getItem('healthTracker')
const lsO = JSON.parse(ls)
// const timeScales = ['hour', 'day', 'month']
let timeScale = 'hour'
let chartTitle = ''

const getChartData = () => {
  const recordTitle = recordTypes.find((obj) => { return obj.name === chartTitle.replace('-', ' ') })
  const title = recordTitle.name.replace(' ', '-')
  if (lsO && lsO[recordTitle.name] && lsO.setup && lsO.setup.tracking.indexOf(recordTitle.name) > -1) {
    $('#periodSelector').removeClass('invisible')
    $('#chartelector').removeClass('invisible')
    let time = lsO[recordTitle.name].time
    let dataSet = lsO[recordTitle.name].value
    $(`#chartSelector-${title}`).parent().addClass('active')
    $(`#periodSelector-${timeScale}`).parent().addClass('active')
    const averages = getAverages(time, dataSet, timeScale)
    drawChart(averages.dateValues, averages.dataSet, recordTitle.min, recordTitle.max, chartTitle, timeScale)
  } else {
    $('#chart').html(`<div class="display-3 mx-auto">No data has been recorded yet</div>`)
    $('#periodSelector').addClass('invisible')
  }
}

const drawChart = (time, data, min, max, div, timeScale) => {
  $('#chart').html(`<canvas id=${div}-${timeScale}-chart class="chart"></canvas>`)
  const ctx = document.getElementById(`${div}-${timeScale}-chart`).getContext('2d')
  let myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: time,
      datasets: [{
        data: data,
        backgroundColor: function (context) {
          var index = context.dataIndex
          return index % 2 ? 'rgba(54, 162, 235, 0.4)' : 'rgba(54, 162, 235, 0.6)'
        },
        borderColor: 'rgba(54, 162, 235, 1)',
        borderWidth: 1
      }]
    },
    options: {
      title: {
        display: false,
        text: div + ' - ' + timeScale
      },
      legend: {
        display: false
      },
      tooltips: {
        callbacks: {
            title: (tooltipItem, data) => {
              if (timeScale === 'hour') {
                return new Date(tooltipItem[0].xLabel).toLocaleTimeString()
              } else {
                return new Date( tooltipItem[0].xLabel).toLocaleDateString()
              }     
            },
            label: (tooltipItem, data) => {
              return Math.round(10 * tooltipItem.yLabel)  / 10
            }
        }
    },
      scales: {
        yAxes: [{
          ticks: {
            min: min,
            max: max
          }
        }],
        xAxes: [{
          type: 'time',
          time: {
            parser: 'YYYY-MM-DD HH:mm:ss',
            unit: timeScale,
            displayFormats: {
              'hour': 'HH:00 ddd',
              'day': 'DD MMM',
              'month': 'MMM'
            }
          },
          offset: true,
          ticks: {
            source: 'data'
          }
        }]
      }
    }
  })
}

const getAverages = (dates, values, period) => {
  let dateValues = []
  const now = new Date()
  let initialDate
  if (period === 'hour') {
    initialDate = new Date(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours() + 1)
    dateValues = [...Array(25).keys()].map(x => new Date(new Date(initialDate).setHours(initialDate.getHours() - x)))
  } else if (period === 'day') {
    initialDate = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1)
    dateValues = [...Array(8).keys()].map(x => new Date(new Date(initialDate).setDate(initialDate.getDate() - x)))
  } else if (period === 'month') {
    initialDate = new Date(now.getFullYear(), now.getMonth() + 1, 1)
    dateValues = [...Array(13).keys()].map(x => new Date(new Date(initialDate).setMonth(initialDate.getMonth() - x)))
  }
  const results = {
    dateValues: dateValues.slice(1), //, dateValues.length - 1),
    dataSet: getDataSet(dates, values, dateValues, period)
  }
  return results
}

const getDataSet = (dates, values, dateValues, period) => {
  let results = [0]
  for (let [i, thisDate] of dateValues.entries()) {
    if (dateValues[i + 1]) {
      let nextDate = dateValues[i + 1]
      let currentCount = 0
      let currentTotal = 0
      for (let j in dates) {
        if (new Date(dates[j]) < thisDate && new Date(dates[j]) > nextDate) {
          currentCount++
          currentTotal += parseFloat(values[j])
        }
      }
      currentCount === 0 ? results.push(0) : results.push(currentTotal / currentCount)
    }
  }
  return results.slice(1)
}

let html = `<div class="btn-group btn-group-toggle" data-toggle="buttons" role="group" aria-label="Chart Selector">`
let records = []
if(lsO.setup && lsO.setup.tracking) {
  for (let record of lsO.setup.tracking) {
    records.push(recordTypes.find((obj) => { return obj.name === record }))
  }
} else {
  records = recordTypes
}
console.log(records)
console.log(lsO.setup.tracking)
for (let chartTitle of records) {
  const title = chartTitle.name.replace(' ', '-')
  if (lsO && lsO.setup && lsO.setup.tracking.indexOf(chartTitle.name) > -1) {
    html += `<label class="btn btn-secondary m-1">`
    html += `<input type="radio" name="chartSelectorRadio" autocomplete="off" id="chartSelector-${title}">${chartTitle.name}`
    html += `</label>`
  }
}
html += '</div>'
$('#chartSelector').html(html)
chartTitle = lsO.setup.tracking[0]
getChartData()
$('#chartSelector :input').on('change', () => {
  chartTitle = $("input[name='chartSelectorRadio']:checked")[0].id.substring(14)
  getChartData()
})
$('#periodSelector :input').on('change', () => {
  timeScale = $("input[name='periodSelectorRadio']:checked")[0].id.substring(15)
  getChartData()
})