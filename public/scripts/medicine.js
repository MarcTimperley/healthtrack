let ls = window.localStorage.getItem('healthTracker')
let lsO = JSON.parse(ls)

// TODO: [] get from setup
const saveMedicine = (medicine) => {
    if (medicine) {
        if (!lsO) return null
        if (!lsO.medicines) lsO.medicines = []
        lsO.medicines.unshift({ date: new Date(), medicineName: medicine })
        window.localStorage.setItem('healthTracker', JSON.stringify(lsO))
        ls = window.localStorage.getItem('healthTracker')
        getMedicines()
    }
}

let html = '<option selected>Select a medicine...</option>'
let selectedMedicines = []
if (lsO && lsO.setup && lsO.setup.medicines) {
lsO.setup.medicines.forEach(element => {
    html += `<option value="${element}">${element}</option>`
})
}
$('#medicineSelect').html(html)
$('#medicineSubmit').on('click', () => {
    saveMedicine($('#medicineSelect :selected').val())
})

const getMedicines = () => {
    if (!lsO || !lsO.medicines) {
        return null
    }
    const medicines = lsO.medicines
    let index = medicines.length
    let medicinesDisplay = [...medicines]
    if (index > 15) medicinesDisplay = medicinesDisplay.slice(0,14)
    console.log(medicinesDisplay)
    let html = ''
    medicinesDisplay.forEach(element => {
        html += `<tr><th scope="row">${index}</th>
      <td>${new Date(element.date).toLocaleDateString()}</td>
      <td>${new Date(element.date).toLocaleTimeString()}</td>
      <td>${element.medicineName}</td></tr>`
      index--
    })
    $('#medicineHistory').html(html)
}

getMedicines()
